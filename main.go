package main

import (
	"gitee.com/gzhtty_1824517828/gin-middleware/handlers/user"
	"gitee.com/gzhtty_1824517828/gin-middleware/middleware"
	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	//依次注入1、2、3中间件
	r.Use(middleware.MidOne(), middleware.MidTwo(), middleware.MidThree())
	v1 := r.Group("/api/v1")
	{
		v1.GET("/:id", user.GetUserInfo)
	}
	r.Run(":8889")
}
