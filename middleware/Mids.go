package middleware

import (
	"fmt"
	"github.com/gin-gonic/gin"
)

// MidOne 中间件1
func MidOne() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fmt.Println("mid1 start")
		ctx.Next()
		//ctx.Abort()
		fmt.Println("mid1 end")
	}
}

// MidTwo 中间件2
func MidTwo() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fmt.Println("mid2 start")
		//ctx.Next()
		fmt.Println("mid2 end")
	}
}

// MidThree 中间件3
func MidThree() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		fmt.Println("mid3 start")
		//ctx.Next()
		fmt.Println("mid3 end")
	}
}
