package user

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
)

func GetUserInfo(ctx *gin.Context) {
	pathId := ctx.Param("id")
	fmt.Println("pathId =", pathId)
	ctx.JSON(http.StatusOK, pathId)
}
